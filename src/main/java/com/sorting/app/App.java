package com.sorting.app;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * This class provides functionality to sort an array of integers in ascending order.
 * It takes command-line arguments as input, sorts them, and prints the sorted array.
 */

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        if (args.length > 10) {
            logger.info("Please provide ten or fewer numbers.");
            return;
        }

        try {
            // Convert command-line arguments to integers
            int[] numbers = Arrays.stream(args)
                    .mapToInt(Integer::parseInt)
                    .toArray();

            // Sort the numbers
            sortNumbers(numbers);

            // Print the sorted numbers
            logger.info("Sorted numbers:");
            for (int number : numbers) {
                System.out.println(number);
            }
        } catch (NumberFormatException e) {
            logger.info("Please ensure all inputs are valid integers.");
        }
    }

    public static void sortNumbers(int[] numbers) {
        // This method sorts numbers in array by ascending order
        boolean numberSwapped;
        do {
            numberSwapped = false;
            for (int i = 0; i < numbers.length - 1; i++) {
                if (numbers[i] > numbers[i + 1]) {
                    // Swap the numbers
                    int temp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = temp;
                    numberSwapped = true;
                }
            }
        } while (numberSwapped);
    }
}
