package com.sorting.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class AppTest {

    private int[] input;
    private int[] expected;

    public AppTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                // Test with zero elements
                { new int[]{}, new int[]{} },
                // Test with one element
                { new int[]{1}, new int[]{1} },
                // Test with ten elements
                { new int[]{10, 3, 2, 5, 4, 7, 6, 9, 8, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} },
                // Existing tests
                { new int[]{4, 3, 2, 1}, new int[]{1, 2, 3, 4} },
                { new int[]{-1, 1, -2, 2}, new int[]{-2, -1, 1, 2} },
                // Add a test case for more than ten elements if your method is expected to handle it
                // This case assumes your method can handle more than ten elements without throwing an error
                // If your method should throw an exception for more than ten elements, this test case would need to be adjusted
                { new int[]{11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11} },
        });
    }

    @Test
    public void testSortNumbers() {
        // Assuming sortNumbers modifies the input array in-place and does not return a new array
        App.sortNumbers(input);
        assertArrayEquals("The sorted array did not match the expected output.", expected, input);
    }
}
